#!/bin/bash

# make start.sh executable to run app with PM2`chmod +x start.sh`
# start by `./start.sh name_of_process`
pm2 start node_modules/react-scripts/scripts/start.js --name $1
