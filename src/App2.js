import React  from 'react';
import './App2.css';
import './pyro.css';
import './border.css';

///// debug
var balance = 0; 			// set balance to start with
var numberOfReels = 3; 		// set number of reels (not recomended over 4)
var debug = 1; 				// set debug to 0 to turn it off
var SetluckieNumber = null 	//set null if want to get random luckie number
/////

var reel;
var slots = [];
var slotsLength = 5;
var slotSequence = [];

var styles = {
  input: {
    marginTop: "4px"
  },
  input_div: {
    textAlign: "center"
   },
   pyroTop: {
   	zIndex: "20",
   	display: "none"
   }
}

class App extends React.Component {

constructor(props) {
  super(props);
  this.state = {
    number: null,
    rolls: [],
  }
  this.go = this.go.bind(this)
  this.addSlots = this.addSlots.bind(this);
  this.slotloop = this.slotloop.bind(this);
  this.moveSlots = this.moveSlots.bind(this);
  this.removeSlots = this.removeSlots.bind(this);
  this.tripleSlots = this.tripleSlots.bind(this);
  this.sleep = this.sleep.bind(this);
  this.shuffle = this.shuffle.bind(this);
  this.wins = this.wins.bind(this);
};


	go () {
		this.moveSlots();
		this.tripleSlots();
	}


	slotloop (array) {
		for (let j = 0; j < slotsLength; j++) {
			var array = [0,1,2,3,4];
			var arrays = [];
			for (var i = 0; i <= array.length; i++) {
				var array = array.sort((array) => Math.random() * 0.5);
				arrays.push(array);
			}
			var img;
			if (j === array[0]) img ="./BAR.png";
			if (j === array[1]) img ="./2xBAR.png";
			if (j === array[2]) img ="./3xBAR.png";
			if (j === array[3]) img ="./7.png";
			if (j === array[4]) img ="./Cherry.png";
	      	var slotId = 'slots_'+j;
	        slots.push(<div id={`${slotId}`} value={`${j}`}><img src={`${img}`}></img></div>)
	// ` <-- added becouse back-tic messes up with sublime ide
	    }
	   	return slots
	}

	addSlots () {
		var array = array;
	    reel = []
	    for (let i = 0; i < numberOfReels; i++) {
	      	this.slotloop();
	      		reel.push(<div id={`${'reel_'+i}`} className='slots content line'><div id={`${'animation_'+i}`}>{slots}</div></div>);
	// ` <-- added becouse back-tic messes up with sublime ide
	   	}
	    return reel
	}



	moveSlots (rolls) {
		var numbers = [];
		var luckieNumbers = [];
		for (var i = 0; i < reel.length; i++){
			var luckieNumber = Math.floor(Math.random() * slots.length/3);
			luckieNumbers.push(luckieNumber);
			if (debug === 1 && SetluckieNumber !== null) luckieNumber = SetluckieNumber;
			var reelToMove =  document.getElementById("animation_"+[i]);
			document.getElementById("go").disabled = true;
			reelToMove.setAttribute("class", "slots slide"+luckieNumber);
			this.removeSlots();
			numbers.push(luckieNumber);
			if (debug === 1 ) document.getElementById("numbers").innerHTML = numbers;
			if (debug === 1 ) document.getElementById("slotSequence").innerHTML = slotSequence;
		}
		document.getElementById("luckieNumber").innerHTML = luckieNumbers;
		//check WINS
		function allEqual(arr) {
		  return new Set(arr).size == 1;
		}
		/// WINS
			if (slotSequence[0] !== undefined) {
				if (allEqual(numbers) === true && numbers[0] === 0){
					this.wins(slotSequence[0]);
				}
				if (allEqual(numbers) === true && numbers[0] === 1){
					this.wins(slotSequence[1]);
				}
				if (allEqual(numbers) === true && numbers[0] === 2){
					this.wins(slotSequence[2]);
				}
				if (allEqual(numbers) === true && numbers[0] === 3){
					this.wins(slotSequence[3]);
				}
				if (allEqual(numbers) === true && numbers[0] === 4){
					this.wins(slotSequence[4]);
				}
				if (numbers[0] === 2) {
					this.wins(slotSequence[2]+"center");
				}
				if (numbers.includes(slotSequence[2]) && numbers.includes(slotSequence[4])) {
					this.wins("cherryAnd7Center");
				}
				if (numbers[0] === slotSequence[0] ||
					numbers[0] === slotSequence[1] ||
					numbers[0] === slotSequence[2] &&
					numbers[1] === slotSequence[0] ||
					numbers[1] === slotSequence[1] ||
					numbers[1] === slotSequence[2] &&
					numbers[2] === slotSequence[0] ||
					numbers[2] === slotSequence[1] ||
					numbers[2] === slotSequence[2]) {
					this.wins("anyBarInCenter");
				}
			}
	}

	wins (id) {
		if (id === "slots_0") {
			balance += 10;
			var rolls = document.getElementById("rolls");
			rolls.innerHTML = balance;
			document.getElementById("wins").innerHTML += "<br> won with " + id + " " + balance + "<br />"
		}
		if (id === "slots_1") {
			balance += 20;
			var rolls = document.getElementById("rolls");
			rolls.innerHTML = balance;
			document.getElementById("wins").innerHTML += "won with " + id + " " + balance + "<br />"
		}
		if (id === "slots_2") {
			balance += 100;
			var rolls = document.getElementById("rolls");
			rolls.innerHTML = balance;
			document.getElementById("wins").innerHTML += "won with " + id + " " + balance + "<br />"
		}
		if (id === "slots_2center") { // 7 on centerline
			balance += 150;
			var rolls = document.getElementById("rolls");
			rolls.innerHTML = balance;
			document.getElementById("wins").innerHTML += "won with " + id + " " + balance + "<br />"
		}
		if (id === "cherryAnd7Center") { // 7 on centerline
			balance += 75;
			var rolls = document.getElementById("rolls");
			rolls.innerHTML = balance;
			document.getElementById("wins").innerHTML += "won with " + id + " " + balance + "<br />"
		}
		if (id === "anyBarInCenter") { // 7 on centerline
			balance += 5;
			var rolls = document.getElementById("rolls");
			rolls.innerHTML = balance;
			document.getElementById("wins").innerHTML += "won with " + id + " " + balance + "<br />"
		}
		if (id === "slots_3") {
			balance += 100;
			var rolls = document.getElementById("rolls");
			rolls.innerHTML = balance;
			document.getElementById("wins").innerHTML += "won with " + id + " " + balance + "<br />"
		}
		if (id === "slots_4") { // Cherry
			balance += 1000;
			var rolls = document.getElementById("rolls");
			rolls.innerHTML = balance;
			document.getElementById("wins").innerHTML += "won with " + id + " " + balance + "<br />"
		}

	}

	sleep(milliseconds) {
        var timeStart = new Date().getTime();
        while (true) {
            var elapsedTime = new Date().getTime() - timeStart;
            if (elapsedTime > milliseconds) {
                break;
            }
        }
     }

	removeSlots () {
		setTimeout(function(){
			for (var i = 0; i < reel.length; i++) {
				var reelToRem = document.getElementById("animation_"+[i]);
				reelToRem.setAttribute("class", "slots slideUp");
				setTimeout(function(reelToRem){
					for (var j = 0; j < reel.length; j++) {
						var reelToRem = document.getElementById("animation_"+[j]);
						var children = reelToRem.childNodes;
						while (children.length === children.length*3) {
			            	reelToRem.removeChild(reelToRem.firstChild);
			            }
					}
				},2000);
			}
			document.getElementById("go").disabled = false;
		}, 5000);
	}

	tripleSlots () {
		for (var i = 0; i <= reel.length-1; i++) {
			var reelToAdd = document.getElementById("animation_"+i);
			var children = reelToAdd.childNodes;
			children.forEach(function(item){
			  var cln = item.cloneNode(true);
			  reelToAdd.appendChild(cln);
			});
		}
	}

	shuffle (array) {
	  array.sort(() => Math.random() - 0.5);
	  return array;
	}


componentDidMount() {
    this.componentDidUpdate()
}

componentDidUpdate() {
    this.go();
    var slide0 = document.getElementById("animation_2").children[25].id;
    var slide1 = document.getElementById("animation_2").children[27].id;
    var slide2 = document.getElementById("animation_2").children[28].id;
    var slide3 = document.getElementById("animation_2").children[23].id;
    var slide4 = document.getElementById("animation_2").children[24].id;
    slotSequence.push(slide0, slide1, slide2, slide3, slide4)
	document.getElementById("rolls").innerHTML = balance;

	if (debug === 1) document.getElementById("debug").setAttribute("class", " ");
}
render() {


var input_div = styles.input_div;
var input = styles.input;
var pyroTop = styles.pyroTop;
var slot = slot;
  return (
      <div id="root">
      	<div id="upper" className="row">
      	</div>
      	<div className="row">
		    <div class="col-sm">
		    </div>
		    <div class="col-sm">
		      	<div className="wrap">
			      	<div className="container">
					</div>
					<div id="slots_container">
						<div className="d-flex justify-content-center box">
							{this.addSlots()}
						</div>
					</div>
		      	</div>
		      	<div class="container2">
				<h2><span>YOUR BALANCE IS <span id="rolls"></span></span></h2>
				</div>
				<div id="debug" className="hidden">
					<p>luckieNumbers = <span id="luckieNumber"></span></p>
					<p>numbers = <span id="numbers"></span></p>
					<p>slotSequence = <span id="slotSequence"></span></p>
					<p>wins = <p id="wins" className="scroll"></p></p>
				</div>
		    </div>
		    <div class="col-sm">
		      	<div style={{input_div}}>
			        <input
			        	type="button"
			        	onClick={ (e) => this.go()}
			        	value="spin" id="go" style={{input}}
			        	className="button align-self-end">
			        </input>
		    	</div>
		    </div>
		</div>
    </div>
    );
  }
}


export default App;